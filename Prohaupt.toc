\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}
\contentsline {section}{\numberline {2}Theorie}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Helmholtzspule}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Fadenstrahlrohr}{1}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Lorentzkraft}{2}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Berechnung der spezifischen Elektronenladung}{2}{subsection.2.4}
\contentsline {section}{\numberline {3}Durchf\IeC {\"u}hrung}{3}{section.3}
\contentsline {section}{\numberline {4}Auswertung}{4}{section.4}
\contentsline {subsection}{\numberline {4.1}Konstanter Strom}{5}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Konstante Beschleunigungsspannung}{6}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Auftragung der Endergebnisse}{6}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Berechnung des Magnetfeldes}{7}{subsection.4.4}
\contentsline {section}{\numberline {5}Diskussion}{8}{section.5}
\contentsline {section}{Literatur}{10}{section*.7}
