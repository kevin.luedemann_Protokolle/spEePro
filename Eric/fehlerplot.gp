reset
set term epslatex color solid
set output "fehlerplot.tex"
set xlabel "Radius $r$ [mm]"
set ylabel "$\\nicefrac{e}{m}$ [$10^{11}\\;$C\\;kg$^{-1}$]"
f(x)=1.758820088
p "fehlerplot1.dat" u 1:3:2:4 t 'Messreihe 1' with xyerrorbars lt rgb "#dd0000" lw 3 ,"fehlerplot2.dat" u 1:3:2:4 t 'Messreihe 2' with xyerrorbars lt rgb "#00bb00" lw 3,"fehlerplot3.dat" u 1:3:2:4 t 'Messreihe 3' with xyerrorbars lt rgb "#0000bb" lw 3,"fehlerplot4.dat" u 1:3:2:4 t 'Messreihe 4' with xyerrorbars lt rgb "#000000" lw 3, f(x) t 'Literaturwert' lt rgb "#550000" lw 3
set output 
!epstopdf fehlerplot.eps
